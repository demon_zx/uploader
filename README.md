# README #

Maven plugin for upload artifact to ftp or sftp server.

### Install ###


```
#!xml

<dependency>
    <groupId>ru.java.fun</groupId>
    <artifactId>uploader</artifactId>
    <version>1.04</version>
</dependency>
```


### Example ###

```
#!xml
<plugin>
    <groupId>ru.java.fun</groupId>
    <artifactId>uploader</artifactId>
    <version>1.04</version>
    <executions>
        <execution>
            <id>upload</id>
            <phase>install</phase>
            <goals>
                <goal>upload</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
        <url>sftp://localhost:22</url>
        <dir>versions</dir>
        <subdirAsDate>true</subdirAsDate>
        <subdir>yyyy-MM-dd</subdir>
        <server>my-ftp-server</server>
        <sourceFileName>${project.artifactId}-${version}.jar</sourceFileName>
        <destinationFileName>test.jar</destinationFileName>
    </configuration>
</plugin>

```

### Configuration ###

* **url** - link to server sftp or ftp protocol;
* **dir** - directory on server relative from user home or absolute path;
* **subdirAsDate** - create subdir with name by date: true or false (optional);
* **subdir** - if subdirAsDate==true then subdir contains date format ([SimpleDateFormat](https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html)), else subdir name;
* **sourceFileName** - file name in target dir;
* **destinationFileName** - destination file name (optional);
* **server** - server from maven config (conf/settings.xml) section servers, example:
```
#!xml

<server>
    <id>my-ftp-server</id>
    <username>ftpuser</username>
    <password>ftppassword</password>
</server>
```