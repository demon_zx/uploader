package ru.java.fun.uploader;

import java.io.InputStream;

/**
 * @author Terentjev Dmitry
 *         Date: 24.04.2015
 *         Time: 11:19
 */
public interface Uploader {

    void uploadFile(InputStream input,String sourceName, String outputDir, String outputName) throws Exception;
}
