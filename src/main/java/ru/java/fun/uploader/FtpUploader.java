package ru.java.fun.uploader;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.maven.plugin.logging.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Terentjev Dmitry
 *         Date: 24.04.2015
 *         Time: 11:22
 */
public class FtpUploader implements Uploader {

    private FTPClient client;
    private Log log;

    public FtpUploader(FTPClient client, Log log) {
        this.client = client;
        this.log = log;
    }

    public void uploadFile(InputStream input, String sourceName, String outputDir, String outputName) throws Exception {
        if(!client.changeWorkingDirectory(outputDir)){
            if(!client.makeDirectory(outputDir)){
                throw new IOException("Can't create directory "+outputDir);
            }
        }
        client.setFileType(FTP.BINARY_FILE_TYPE);
        for (FTPFile ftpFile : client.listFiles()) {
            if(ftpFile.getName().equals(outputName)){
                log.info("File "+outputName+" exist. Delete...");
                client.deleteFile(outputName);
            }
        }
        log.info("Upload file " + sourceName + " -> " + outputDir + outputName);
        client.appendFile(outputName,input);
    }
}
