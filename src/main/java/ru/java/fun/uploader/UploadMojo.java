package ru.java.fun.uploader;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Terentjev Dmitry
 *         Date: 24.04.2015
 *         Time: 9:13
 */
@Mojo(name = "upload", defaultPhase = LifecyclePhase.INSTALL, threadSafe = true)
public class UploadMojo extends AbstractMojo {

    @Component
    protected MavenProject project;
    @Component
    protected MavenSession session;
    @Component
    protected BuildPluginManager buildManager;
    @Parameter(required = true)
    String url;
    @Parameter
    String dir;
    @Parameter
    String subdir;
    @Parameter(defaultValue = "false")
    String subdirAsDate;
    @Parameter
    String server;
    @Parameter(required = true)
    String sourceFileName;
    @Parameter
    String destinationFileName;
    @Parameter(defaultValue = "${settings}", readonly = true)
    private Settings settings;

    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            Server selectedServer = null;
            for (Server s : settings.getServers()) {
                if (s.getId().contains(server)) {
                    selectedServer = s;
                }
            }
            String user = null, pass = null;
            if (selectedServer != null) {
                getLog().info("Find server: " + selectedServer.getId());
                user = selectedServer.getUsername();
                pass = selectedServer.getPassword();
            }
            if (user == null) {
                System.out.println("Request user and password");
            }
            if (url != null) {
//                for (String url : urls) {
                if (url.toLowerCase().startsWith("sftp://")) {
                    uploadSftp(user, pass, url);
                }
//                }
                if (url.toLowerCase().startsWith("ftp://")) {
                    uploadFtp(user, pass, url);
                }
            }
        } catch (Exception ioe) {
            throw new MojoFailureException(ioe.getMessage());
        }
    }


    private void uploadSftp(String user, String password, String url) throws Exception {
        Url link = new Url(url);


        final File file = new File("./target/" + sourceFileName);

        String destination = destinationFileName == null || destinationFileName.trim().isEmpty() ? sourceFileName : destinationFileName;

        JSch ssh = new JSch();
        File known = new File(System.getProperty("user.home") + "/.jsch");
        if (!known.exists()) {
            known.createNewFile();
        }
        ssh.setKnownHosts(known.getAbsolutePath());
        Session session = ssh.getSession(user, link.getHost(), link.getPort());
        session.setPassword(password);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
        Channel chan = session.openChannel("sftp");
        chan.connect();
        ChannelSftp sftp = (ChannelSftp) chan;
//            InputStream is = new CountingInputStream(new FileInputStream(file)) {
//                @Override
//                protected synchronized void afterRead(int n) {
//                    super.afterRead(n);
//                    getLog().info('\r');
//                    print(srcName + ": " + ((int) ((getCount() / file.length()) * 100)) + "%   ");
//                }
//            }
        InputStream is = new FileInputStream(file);

        String currentDir = dir.startsWith("/") ? "" : sftp.pwd() + "/";
        String workDir = currentDir + dir + (dir.endsWith("/") ? "" : "/");
        if (subdir != null) {
            String sd = subdir;
            if (subdirAsDate != null && Boolean.parseBoolean(subdirAsDate)) {
                sd = new SimpleDateFormat(subdir).format(new Date());
            }
            workDir += sd + "/";
        }
        Uploader upl = new SftpUploader(sftp, getLog());
        upl.uploadFile(is, sourceFileName, workDir, destination);

        sftp.disconnect();
        session.disconnect();
    }

    private void uploadFtp(String user, String password, String url) throws Exception {
        Url link = new Url(url);
        final File file = new File("./target/" + sourceFileName);
        String destination = destinationFileName == null || destinationFileName.trim().isEmpty() ? sourceFileName : destinationFileName;
        InputStream is = new FileInputStream(file);

        FTPClient ftp = new FTPClient();
        ftp.connect(link.getHost());
        ftp.enterLocalPassiveMode();
        ftp.login(user, password);
        String currentDir = dir.startsWith("/") ? "" : ftp.printWorkingDirectory() + "/";
        String workDir = currentDir + dir + (dir.endsWith("/") ? "" : "/");
        if (subdir != null) {
            String sd = subdir;
            if (subdirAsDate != null && Boolean.parseBoolean(subdirAsDate)) {
                sd = new SimpleDateFormat(subdir).format(new Date());
            }
            workDir += sd + "/";
        }

        Uploader upl = new FtpUploader(ftp, getLog());
        upl.uploadFile(is, sourceFileName, workDir, destination);

        ftp.disconnect();

    }

}
