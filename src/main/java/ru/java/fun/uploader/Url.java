package ru.java.fun.uploader;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Terentjev Dmitry
 *         Date: 28.04.2015
 *         Time: 13:49
 */
public class Url {

    private String url,protocol;

    public static final String FTP = "FTP";
    public static final String SFTP = "SFTP";

    public Url(String url) throws MalformedURLException {
        this.url = url;
        Pattern p = Pattern.compile("(\\w+)://(.+?)(:([\\d]{1,5}))?$");
        Matcher m = p.matcher(url);
        if(m.matches()){
            protocol = m.group(1).toUpperCase();
            host = m.group(2);
            String sPort = m.group(4);
            port = sPort == null ? getDefaultPort() : Integer.parseInt(sPort);
        }else {
            throw new MalformedURLException();
        }
    }

    private int getDefaultPort(){
        if(protocol.equals(FTP)){
            return 21;
        }
        if(protocol.equals(SFTP)){
            return 22;
        }
        return -1;
    }
    private String host;
    private int port;

    public String getUrl() {
        return url;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return url;
    }
}
