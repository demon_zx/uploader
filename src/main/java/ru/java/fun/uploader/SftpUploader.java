package ru.java.fun.uploader;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;
import org.apache.maven.plugin.logging.Log;

import java.io.InputStream;

/**
 * @author Terentjev Dmitry
 *         Date: 24.04.2015
 *         Time: 11:22
 */
public class SftpUploader implements Uploader {

    private ChannelSftp sftp;
    private Log log;

    public SftpUploader(ChannelSftp sftp, Log log) {
        this.sftp = sftp;
        this.log = log;
    }

    public void uploadFile(InputStream input, String sourceName, String outputDir, String outputName) throws Exception {
        SftpATTRS attrs = null;
        try {
            attrs = sftp.stat(outputDir);
        } catch (Exception ignored) {
        }
        if (attrs == null) {
            log.info("mkdir: " + outputDir);
            sftp.mkdir(outputDir);
        }
        log.info("upload: " + sourceName + " -> " + outputDir + outputName);
        sftp.put(input, outputDir + outputName);
    }
}
